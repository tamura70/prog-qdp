#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Queen Domination Problem
https://oeis.org/A075458
https://mathworld.wolfram.com/QueenGraph.html
https://mathworld.wolfram.com/DominationNumber.html

n	γ(Q_n)
10	5
11	5
12	6
13	7
14	8
15	9
16	9
17	9
18	9
19	10
20	10-11
21	11
22	11-12
23	12
"""

import sys
import getopt
from util import *

# オプション
opts = dict()

"""
モデル0
"""
def model0(n, k):
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c("int", v("q",i,j), 0, 1))
    qs = [v("q",i,j) for i in range(1, n+1) for j in range(1, n+1)]
    atmost_k(k, qs, opts)
    for i in range(1, n+1):
        for j in range(1, n+1):
            q_row = [v("q",i,j1) for j1 in range(1, n+1)]
            q_col = [v("q",i1,j) for i1 in range(1, n+1)]
            q_up = [v("q",i1,-i1+i+j) for i1 in range(1, n+1) if 1 <= -i1+i+j and -i1+i+j <= n]
            q_down = [v("q",i1,i1-i+j) for i1 in range(1, n+1) if 1 <= i1-i+j and i1-i+j <= n]
            qs = q_row + q_col + q_up + q_down
            # cs = [c(">", q, 0) for q in qs]
            # print(c("or", " ".join(cs)))
            print(c(">", c("+", " ".join(qs)), 0))

"""
モデル1
"""
def model1(n, k):
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c("int", v("q",i,j), 0, 1))
    for i in range(1, n+1):
        qs = [v("q",i,j1) for j1 in range(1, n+1)]
        print(c("int", v("r",i), 0, 1))
        print(c("imp", c(">",v("r",i),0), c(">",c("+", " ".join(qs)),0)))
    for j in range(1, n+1):
        qs = [v("q",i1,j) for i1 in range(1, n+1)]
        print(c("int", v("c",j), 0, 1))
        print(c("imp", c(">",v("c",j),0), c(">",c("+", " ".join(qs)),0)))
    for a in range(1, 2*n):
        # a = i1+j1-1
        qs = [v("q",i1,a-i1+1) for i1 in range(1, n+1) if 1 <= a-i1+1 and a-i1+1 <= n]
        print(c("int", v("u",a), 0, 1))
        print(c("imp", c(">",v("u",a),0), c(">",c("+", " ".join(qs)),0)))
    for b in range(1, 2*n):
        # b = i1-j1+n
        qs = [v("q",i1,-b+i1+n) for i1 in range(1, n+1) if 1 <= -b+i1+n and -b+i1+n <= n]
        print(c("int", v("d",b), 0, 1))
        print(c("imp", c(">",v("d",b),0), c(">",c("+", " ".join(qs)),0)))
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c(">", c("+",v("r",i),v("c",j),v("u",i+j-1),v("d",i-j+n)), 0))
    #
    qs = [v("q",i,j) for i in range(1, n+1) for j in range(1, n+1)]
    print(c("=", c("+", " ".join(qs)), k))

"""
モデル2
"""
def model2(n, k):
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c("int", v("q",i,j), 0, 1))
    for i in range(1, n+1):
        qs = [v("q",i,j1) for j1 in range(1, n+1)]
        print(c("int", v("r",i), 0, k))
        print(c("=", v("r",i), c("+", " ".join(qs))))
    for j in range(1, n+1):
        qs = [v("q",i1,j) for i1 in range(1, n+1)]
        print(c("int", v("c",j), 0, k))
        print(c("=", v("c",j), c("+", " ".join(qs))))
    for a in range(1, 2*n):
        # a = i1+j1-1
        qs = [v("q",i1,a-i1+1) for i1 in range(1, n+1) if 1 <= a-i1+1 and a-i1+1 <= n]
        print(c("int", v("u",a), 0, k))
        print(c("=", v("u",a), c("+", " ".join(qs))))
    for b in range(1, 2*n):
        # b = i1-j1+n
        qs = [v("q",i1,-b+i1+n) for i1 in range(1, n+1) if 1 <= -b+i1+n and -b+i1+n <= n]
        print(c("int", v("d",b), 0, k))
        print(c("=", v("d",b), c("+", " ".join(qs))))
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c("or", c(">",v("r",i),0), c(">",v("c",j),0), c(">",v("u",i+j-1),0), c(">",v("d",i-j+n),0)))
    #
    rs = [v("r",i) for i in range(1, n+1)]
    print(c("=", c("+", " ".join(rs)), k))
    cs = [v("c",j) for j in range(1, n+1)]
    print(c("=", c("+", " ".join(cs)), k))
    us = [v("u",a) for a in range(1, 2*n)]
    print(c("=", c("+", " ".join(us)), k))
    ds = [v("d",b) for b in range(1, 2*n)]
    print(c("=", c("+", " ".join(ds)), k))

"""
モデル3
"""
def model3(n, k):
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c("int", v("q",i,j), 0, 1))
    m = int(n / 2)
    for i in range(1, n+1):
        r1 = [v("q",i,j1) for j1 in range(1, m+1)]
        r2 = [v("q",i,j1) for j1 in range(m+1, n+1)]
        print(c("int", v("r",1,i), 0, k))
        print(c("=", v("r",1,i), c("+", " ".join(r1))))
        print(c("int", v("r",2,i), 0, k))
        print(c("=", v("r",2,i), c("+", " ".join(r2))))
        print(c("int", v("r",i), 0, k))
        print(c("=", v("r",i), c("+", v("r",1,i), v("r",2,i))))
    for j in range(1, n+1):
        c1 = [v("q",i1,j) for i1 in range(1, m+1)]
        c2 = [v("q",i1,j) for i1 in range(m+1, n+1)]
        print(c("int", v("c",1,j), 0, k))
        print(c("=", v("c",1,j), c("+", " ".join(c1))))
        print(c("int", v("c",2,j), 0, k))
        print(c("=", v("c",2,j), c("+", " ".join(c2))))
        print(c("int", v("c",j), 0, k))
        print(c("=", v("c",j), c("+", v("c",1,j), v("c",2,j))))
    for a in range(1, 2*n):
        # a = i1+j1-1
        qs = [v("q",i1,a-i1+1) for i1 in range(1, n+1) if 1 <= a-i1+1 and a-i1+1 <= n]
        print(c("int", v("u",a), 0, k))
        print(c("=", v("u",a), c("+", " ".join(qs))))
    for b in range(1, 2*n):
        # b = i1-j1+n
        qs = [v("q",i1,-b+i1+n) for i1 in range(1, n+1) if 1 <= -b+i1+n and -b+i1+n <= n]
        print(c("int", v("d",b), 0, k))
        print(c("=", v("d",b), c("+", " ".join(qs))))
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c("or", c(">",v("r",i),0), c(">",v("c",j),0), c(">",v("u",i+j-1),0), c(">",v("d",i-j+n),0)))
    #
    block([v("r",1,i) for i in range(1, m+1)], [v("c",1,j) for j in range(1, m+1)])
    block([v("r",2,i) for i in range(1, m+1)], [v("c",1,j) for j in range(m+1, n+1)])
    block([v("r",1,i) for i in range(m+1, n+1)], [v("c",2,j) for j in range(1, m+1)])
    block([v("r",2,i) for i in range(m+1, n+1)], [v("c",2,j) for j in range(m+1, n+1)])
    #
    rs = [v("r",i) for i in range(1, n+1)]
    print(c("=", c("+", " ".join(rs)), k))
    cs = [v("c",j) for j in range(1, n+1)]
    print(c("=", c("+", " ".join(cs)), k))
    us = [v("u",a) for a in range(1, 2*n)]
    print(c("=", c("+", " ".join(us)), k))
    ds = [v("d",b) for b in range(1, 2*n)]
    print(c("=", c("+", " ".join(ds)), k))

def block(rs, cs):
    print(c("=", c("+", " ".join(rs)), c("+", " ".join(cs))))

"""
モデル4
"""
def model4(n, k):
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c("int", v("q",i,j), 0, 1))
    for i in range(1, n+1):
        qs = [v("q",i,j1) for j1 in range(1, n+1)]
        print(c("int", v("r",i), 0, 1))
        equivOR(v("r",i), qs)
    for j in range(1, n+1):
        qs = [v("q",i1,j) for i1 in range(1, n+1)]
        print(c("int", v("c",j), 0, 1))
        equivOR(v("c",j), qs)
    for a in range(1, 2*n):
        # a = i1+j1-1
        qs = [v("q",i1,a-i1+1) for i1 in range(1, n+1) if 1 <= a-i1+1 and a-i1+1 <= n]
        print(c("int", v("u",a), 0, 1))
        equivOR(v("u",a), qs)
    for b in range(1, 2*n):
        # b = i1-j1+n
        qs = [v("q",i1,-b+i1+n) for i1 in range(1, n+1) if 1 <= -b+i1+n and -b+i1+n <= n]
        print(c("int", v("d",b), 0, 1))
        equivOR(v("d",b), qs)
    for i in range(1, n+1):
        for j in range(1, n+1):
            print(c(">=", c("+",v("r",i),v("c",j),v("u",i+j-1),v("d",i-j+n)), 1))
    #
    qs = [v("q",i,j) for i in range(1, n+1) for j in range(1, n+1)]
    print(c("=", c("+", " ".join(qs)), k))
    #
    rs = [v("r",i) for i in range(1, n+1)]
    print(c("<=", c("+", " ".join(rs)), k))
    cs = [v("c",j) for j in range(1, n+1)]
    print(c("<=", c("+", " ".join(cs)), k))
    us = [v("u",a) for a in range(1, 2*n)]
    print(c("<=", c("+", " ".join(us)), k))
    ds = [v("d",b) for b in range(1, 2*n)]
    print(c("<=", c("+", " ".join(ds)), k))

def equivOR(x, qs):
    print(c(">=", c("+"," ".join(qs)), x))
    for q in qs:
        print(c("<=", q, x))


"""
解の検証
"""
def verify(n, k):
    sol = read_solution()
    print("Verify Sugar output")
    print("solution =", sol)
    ok = True
    qs = [sol[v("q",i,j)] for i in range(1, n+1) for j in range(1, n+1)]
    ok = ok and sum(qs) <= k
    for i in range(1, n+1):
        for j in range(1, n+1):
            q_row = [v("q",i,j1) for j1 in range(1, n+1)]
            q_col = [v("q",i1,j) for i1 in range(1, n+1)]
            q_up = [v("q",i1,-i1+i+j) for i1 in range(1, n+1) if 1 <= -i1+i+j and -i1+i+j <= n]
            q_down = [v("q",i1,i1-i+j) for i1 in range(1, n+1) if 1 <= i1-i+j and i1-i+j <= n]
            qs = [sol[q] for q in q_row + q_col + q_up + q_down]
            ok = ok and sum(qs) >= 1
    if ok:
        print("Verify OK")
    else:
        print("Verify NG")

"""
メイン
"""
def main():
    global opts
    o, args = getopt.getopt(sys.argv[1:], "hvm:", ["bc="])
    opts = dict(o)
    if "-h" in opts or len(args) != 2:
        print("Usage: %s n k" % sys.argv[0])
        return
    n = int(args[0])
    k = int(args[1])
    if "-v" in opts:
        verify(n, k)
        return
    model = opts.get("-m", "0")
    print(f"; QDP モデル{model} n={n} k={k} opts={opts}")
    if model == "0":
        model0(n, k)
    elif model == "1":
        model1(n, k)
    elif model == "2":
        model2(n, k)
    elif model == "3":
        model3(n, k)
    elif model == "4":
        model4(n, k)
    else:
        raise NotImplementedError(f"不明なモデル {model}")

if __name__ == "__main__":
    main()
