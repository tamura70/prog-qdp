#!/bin/sh
# set -x
n=$1
d=$2
file=q$n/qdp0-$n-$d
qdp_jar=target/scala-2.12/qdp_2.12-1.0.jar

mkdir -p q$n
py/qdp.py -m0 $n $d >$file.csp
sugar -n -pb -o no_simp,no_reduce -sat $file.opb $file.csp
clasp -n0 -q0 $file.opb >$file.sol
scala -cp $qdp_jar Trans -m $n <$file.sol >$file-m.graph
scala -cp $qdp_jar Analyze $file-m.graph
scala -cp $qdp_jar Analyze -g -s $file-m.graph | sfdp -Goverlap=false -Ecolor=gray -x -Tsvg >$file-m.svg
exit 0
