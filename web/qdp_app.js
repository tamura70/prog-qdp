let node = 0;
let table = [];
let adjacents = [];

function qdp_refresh() {
    let q = "<table>";
    for (let i = 0; i < qdp_size; i++) {
        q += "<tr>";
        for (let j = 0; j < qdp_size; j++) {
            q += "<td>";
            q += table[i][j] == 0 ? "" : "Q";
            q += "</td>";
        }
        q += "</tr>";
    }
    q += "</table>";
    let a = [];
    for (adj of adjacents) {
        a.push(`<a href="index.html?node=${adj}">${adj}</a>`);
    }
    document.getElementById("name").innerHTML = "Solution " + node;
    document.getElementById("queens").innerHTML = q;
    document.getElementById("adjacents").innerHTML = a.join(", ");
}

function qdp_table(qs) {
    let t = Array(qdp_size);
    for (let i = 0; i < qdp_size; i++) {
        t[i] = Array(qdp_size).fill(0);
    }
    for (let ij of qs) {
        t[ij[0]][ij[1]] = 1;
    }
    return t;
}

function qdp_init() {
    node = 0;
    table = [];
    adjacents = [];
    let s = window.location.search;
    if (s.startsWith("?node=")) {
        node = parseInt(s.substring(6));
        table = qdp_table(qdp_node_map[node]);
        adjacents = qdp_adj_map[node];
    }
    qdp_refresh();
}
