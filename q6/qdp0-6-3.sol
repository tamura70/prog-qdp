c clasp version 3.3.5
c Reading from q6/qdp0-6-3.opb
c Solving...
c Answer: 1
v -x1 -x2 -x3 -x4 -x5 -x6 -x7 -x8 -x9 x10 -x11 -x12 -x13 -x14 -x15 -x16 -x17
v -x18 -x19 x20 -x21 -x22 -x23 -x24 -x25 -x26 -x27 -x28 -x29 -x30 -x31 -x32
v -x33 -x34 -x35 x36
c Answer: 2
v -x1 -x2 -x3 -x4 -x5 -x6 -x7 -x8 x9 -x10 -x11 -x12 -x13 -x14 -x15 -x16 -x17
v -x18 -x19 -x20 -x21 -x22 x23 -x24 -x25 -x26 -x27 -x28 -x29 -x30 x31 -x32
v -x33 -x34 -x35 -x36
c Answer: 3
v -x1 -x2 -x3 -x4 -x5 x6 -x7 -x8 -x9 -x10 -x11 -x12 -x13 x14 -x15 -x16 -x17
v -x18 -x19 -x20 -x21 -x22 -x23 -x24 -x25 -x26 -x27 x28 -x29 -x30 -x31 -x32
v -x33 -x34 -x35 -x36
c Answer: 4
v x1 -x2 -x3 -x4 -x5 -x6 -x7 -x8 -x9 -x10 -x11 -x12 -x13 -x14 -x15 -x16 x17
v -x18 -x19 -x20 -x21 -x22 -x23 -x24 -x25 -x26 x27 -x28 -x29 -x30 -x31 -x32
v -x33 -x34 -x35 -x36
s SATISFIABLE
c 
c Models         : 4
c Calls          : 1
c Time           : 0.011s (Solving: 0.01s 1st Model: 0.00s Unsat: 0.00s)
c CPU Time       : 0.011s
