import jp.kobe_u.copris._
import jp.kobe_u.copris.sugar.dsl._

object LongestCycle {
  var graph: Graph = _
  var opts: Map[String,String] = Map.empty

  def define: Unit = {
    for (v <- graph.nodes)
      boolInt('d(v))
    for ((u,v) <- graph.edges) {
      boolInt('a(u,v))
      boolInt('a(v,u))
      add('a(u,v) + 'a(v,u) <= 1)
    }
    for (v <- graph.nodes) {
      val adj = graph.adj(v)
      add('d(v) === Add(adj.map(u => 'a(u,v))))
      add('d(v) === Add(adj.map(u => 'a(v,u))))
    }
    add(Add(graph.nodes.map(v => 'd(v))) >= 1)
  }

  def getCycles: Set[Seq[Int]] = {
    def getCycle(u: Int, path: Seq[Int]): Seq[Int] =
      if (! path.isEmpty && u == path.head) {
        path
      } else {
        graph.adj(u).filter(v => solution('a(u,v)) > 0).toSeq match {
          case Seq(v) => getCycle(v, path :+ u)
        }
      }
    var cycles: Set[Seq[Int]] = Set.empty
    var remainingNodes = graph.nodes.filter(u => solution('d(u)) > 0)
    while (! remainingNodes.isEmpty) {
      val u = remainingNodes.min
      val cycle = getCycle(u, Seq.empty)
      remainingNodes --= cycle
      cycles += cycle
    }
    cycles
  }

  def addMinimumDegree(degree: Int): Unit = {
    if (opts.contains("-pb") && opts.contains("-d")) {
      val ds = graph.nodes.map(v => 'd(v))
      add(Add(ds) >= degree)
    }
  }

  def disableCycle(cycle: Seq[Int]): Unit = {
    for (cyc <- Seq(cycle, cycle.reverse)) {
      val as = for (Seq(u,v) <- (cyc :+ cyc.head).sliding(2).toSeq) yield 'a(v,u)
      add(Or(as.map(_ === 0)))
    }
  }

  /*
   * Usage: copris lognestCycle [options] <q8/qdp0-8-5-m.dot
   * -s1=satSolver : use SAT solver (e.g. -s1=kissat)
   * -s2=satSolver : use SAT solver (e.g. -s2=minisat)
   */
  def main(args: Array[String]): Unit = {
    val (o,rest) = Util.getOpts(args)
    opts = o
    val g =
      if (rest.size > 0) Graph.loadGraph(rest(0))
      else Graph.loadGraph
    // only consider the largest component.
    graph = g.largestComponent
    println("Options: " + opts)
    println(s"Graph : ${graph.nodes.size} nodes, ${graph.edges.size} edges")
    if (opts.contains("-s1")) {
      use(new sugar.Solver(csp, new sugar.SatSolver1(opts("-s1"))))
    } else if (opts.contains("-s2")) {
      use(new sugar.Solver(csp, new sugar.SatSolver2(opts("-s2"))))
    }

    var longestCycle: Seq[Int] = Seq.empty
    define
    var i = 0
    var total = 0
    if (find) {
      do {
        val cycles = getCycles
        i += 1
        total += cycles.size
        println(s"Iteration $i : ${cycles.size} cycles (total $total cycles, longest ${longestCycle.size})")
        for (cycle <- cycles) {
          if (cycle.size > longestCycle.size) {
            longestCycle = cycle
            val c = cycle.map(v => graph.nodeEquiv(v)).map(ut => ut._1 + "," + ut._2)
            println(s"Cycle length ${cycle.size} : " + c.mkString(" "))
            // addMinimumDegree(cycle.size + 1)
          }
          disableCycle(cycle)
        }
        solver.encodeDelta
	csp.commit
	solver.commit
      } while (solver.satSolve)
    }
  }
}
