class Graph {
  import scala.math.Ordering.Implicits._

  var size = 0
  var nodeMap: Map[Int,Seq[(Int,Int)]] = Map.empty
  var shapeMap: Map[Seq[(Int,Int)],Int] = Map.empty
  var nodeEquiv: Map[Int,(Int,String)] = Map.empty
  var edges: Set[(Int,Int)] = Set.empty

  def addNode(v: Int, shape: Seq[(Int,Int)]): Unit = {
    val s = shape.sorted
    nodeMap += v -> s
    shapeMap += s -> v
  }
  def addEdge(u: Int, v: Int): Unit = {
    if (u < v)
      edges = edges + ((u, v))
    else
      edges = edges + ((v, u))
  }
  def nodes = nodeMap.keySet
  def adj(u: Int): Set[Int] = {
    val vs1 = for ((u1,v1) <- edges; if u1 == u) yield v1
    val vs2 = for ((u1,v1) <- edges; if v1 == u) yield u1
    vs1 union vs2
  }
  def component(u: Int): Set[Int] = {
    def c(open: Set[Int], closed: Set[Int]): Set[Int] = {
      if (open.isEmpty) {
        closed
      } else {
        val vs = adj(open.head) diff closed
        c((open union vs) - open.head, closed + open.head)
      }
    }
    c(Set(u), Set.empty)
  }
  lazy val components: Set[Set[Int]] = {
    var remainingNodes = nodes
    var cs: Seq[Set[Int]] = Seq.empty
    while (! remainingNodes.isEmpty) {
      val c = component(remainingNodes.head)
      remainingNodes = remainingNodes diff c
      cs = c +: cs
    }
    cs.toSet
  }
  lazy val largestComponent: Graph = {
    val m = components.map(_.size).max
    val vs = components.filter(_.size == m).head
    val g = new Graph
    g.size = size
    for (u <- vs) {
      g.addNode(u, nodeMap(u))
      for (v <- adj(u); if u < v && vs.contains(v))
        g.addEdge(u, v)
    }
    g.setSymmetry
    g
  }
  def rotate(s: Seq[(Int,Int)]) = s.map { case (i,j) => (j,size-1-i) }.sorted
  def mirror(s: Seq[(Int,Int)]) = s.map { case (i,j) => (j,i) }.sorted
  def syms(shape: Seq[(Int,Int)]): Seq[(Int,String)] = {
    val i = shape
    val r1 = rotate(shape)
    val r2 = rotate(r1)
    val r3 = rotate(r2)
    val m = mirror(shape)
    val m1 = rotate(m)
    val m2 = rotate(m1)
    val m3 = rotate(m2)
    val st = Seq((i,"I"), (r1,"R1"), (r2,"R2"), (r3,"R3"),
                 (m,"M"), (m1,"M1"), (m2,"M2"), (m3,"M3"))
    st.map { case (s,t) => (shapeMap(s),t) }
  }
  def setSymmetry: Unit = {
    for (v <- nodes) {
      val s = syms(nodeMap(v))
      val u = s.map(_._1).min
      nodeEquiv += v -> s.find(_._1 == u).get
    }
  }
  def output: Unit = {
    setSymmetry
    println(s"Size $size")
    for (v <- nodes.toSeq.sorted) {
      val (s,t) = nodeEquiv(v)
      val shape = nodeMap(v).map(pos => pos._1 + "," + pos._2).mkString(" ")
      println(s"Node $v : $s,$t $shape")
    }
    val es = edges.toSeq.sorted
    for (i <- 0 until es.size) {
      val (u,v) = es(i)
      println(s"Edge ${i+1} : $u $v")
    }
    
  }
}

object Graph {
  def loadGraph(source: scala.io.Source): Graph = {
    val graph = new Graph
    for (line <- source.getLines()) {
      if (line.startsWith("Size ")) {
        graph.size = line.split(" ")(1).toInt
      } else if (line.startsWith("Node ")) {
        val s = line.split(" ")
        val v = s(1).toInt
        val qs = for (Array(x,y) <- s.drop(4).map(_.split(",").map(_.toInt)))
                 yield (x,y)
        graph.addNode(v, qs)
      } else if (line.startsWith("Edge ")) {
        val e = line.split(" ").drop(3)
        graph.addEdge(e(0).toInt, e(1).toInt)
      }
    }
    graph.setSymmetry
    graph
  }
  def loadGraph(fileName: String): Graph =
    loadGraph(scala.io.Source.fromFile(fileName))
  def loadGraph: Graph =
    loadGraph(scala.io.Source.stdin)
}
