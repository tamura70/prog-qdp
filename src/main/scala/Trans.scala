object Trans {
  import scala.math.Ordering.Implicits._

  var opts: Map[String,String] = Map.empty
  var movableOnly = false
  var size = 0

  var solutions: Seq[Seq[(Int,Int)]] = Seq.empty
  var edges: Seq[(Int,Int)] = Seq.empty

  def movable(pos1: (Int,Int), pos2: (Int,Int)) =
    pos1._1 == pos2._1 ||
    pos1._2 == pos2._2 ||
    pos1._1+pos1._2 == pos2._1+pos2._2 ||
    pos1._1-pos1._2 == pos2._1-pos2._2
  def loadSolutions: Unit = {
    def i2pos(i: Int) = ((i-1)/size, (i-1)%size)
    var sol: Seq[(Int,Int)] = Seq.empty
    for (line <- scala.io.Source.stdin.getLines()) {
      if (line.startsWith("v ")) {
        sol ++= line.split(" ").filter(_.startsWith("x")).map(x => i2pos(x.substring(1).toInt))
      } else if (sol.size > 0) {
        solutions = sol.sorted +: solutions
        sol = Seq.empty
      }
    }
    solutions = solutions.sorted
  }
  def findEdges: Unit = {
    val n = solutions.size
    for {
      i <- 0 until n; s1 = solutions(i)
      j <- i+1 until n; s2 = solutions(j)
      if s1.contains(s2(0)) || s1.contains(s2(1))
      if s2.contains(s1(0)) || s2.contains(s1(1))
      d12 = s1 diff s2
      if d12.size <= 1
      d21 = s2 diff s1
      if d21.size <= 1
    } {
      if (movableOnly) {
        (d12, d21) match {
          case (Seq(pos1),Seq(pos2)) if movable(pos1, pos2) => {
            edges = edges :+ (i,j)
          }
          case _ => {
          }
        }
      } else {
        edges = edges :+ (i,j)
      }
    }
  }
  def main(args: Array[String]): Unit = {
    val (o, rest) = Util.getOpts(args)
    opts = o
    size = rest.head.toInt
    movableOnly = opts.contains("-m")
    loadSolutions
    findEdges
    var graph = new Graph
    graph.size = size
    for (i <- 0 until solutions.size) {
      graph.addNode(i+1, solutions(i))
    }
    for (k <- 0 until edges.size) {
      val (i,j) = edges(k)
      graph.addEdge(i+1, j+1)
    }
    graph.output
  }
}
