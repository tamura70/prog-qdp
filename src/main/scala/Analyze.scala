object Analyze {
  var opts: Map[String,String] = Map.empty
  def analyze(graph: Graph): Unit = {
    Console.err.println(s"Nodes = ${graph.nodes.size}, Edges = ${graph.edges.size}")
    val d = graph.nodes.map(v => graph.adj(v).size).max
    val vs = graph.nodes.filter(v => graph.adj(v).size == d).toSeq.sorted
    Console.err.println(s"Largest degree = $d, Nodes = $vs")
    val cs = graph.components
    Console.err.println(s"Components = ${cs.size}")
    val m = cs.map(_.size).max
    Console.err.println(s"Largest component size = $m")
    for (c <- cs; if c.size == m) {
      Console.err.println(s"  Component including ${c.min}")
    }
  }
  def toGraphviz(graph: Graph): Unit = {
    val nodes = graph.nodes.toSeq.sorted
    println("graph {")
    for (v <- nodes) {
      val s = graph.nodeMap(v).map(pos => pos._1 + "," + pos._2).mkString(" ")
      if (opts.contains("-s")) {
        val (u,t) = graph.nodeEquiv(v)
        println(s"""$v [label="$u,$t"];""")
      } else {
        println(s"$v;")
      }
    }
    for (e <- graph.edges) {
      println(s"${e._1} -- ${e._2};")
    }
    println("}")
  }
  def toJson(graph: Graph): Unit = {
    val nodes = graph.nodes.toSeq.sorted
    val ns = for (v <- nodes) yield {
      val d = graph.nodeMap(v).map{ case (i,j) => s"[$i,$j]" }
      v + " : " + d.mkString("[", ",", "]")
    }
    val as = for (v <- nodes) yield {
      v + " : " + graph.adj(v).toSeq.sorted.mkString("[", ",", "]")
    }
    println("let qdp_size = 8;");
    println
    println("let qdp_node_map = {");
    println(ns.mkString("  ", ",\n  ", ""))
    println("};");
    println
    println("let qdp_adj_map = {");
    println(as.mkString("  ", ",\n  ", ""))
    println("};");
    println
  }
  def main(args: Array[String]): Unit = {
    val (o, rest) = Util.getOpts(args)
    opts = o
    val graph =
      if (rest.size > 0) Graph.loadGraph(rest(0))
      else Graph.loadGraph
    if (opts.contains("-g")) {
      toGraphviz(graph)
    } else if (opts.contains("-j")) {
      toJson(graph)
    } else {
      analyze(graph)
    }
  }
}
