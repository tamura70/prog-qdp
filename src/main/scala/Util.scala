object Util {

  def getOpts(args: Seq[String]): (Map[String,String],Seq[String]) = {
    var opts: Map[String,String] = Map.empty
    def _getOpts(args: Seq[String]): Seq[String] = args match {
      case Seq() => {
        Seq.empty
      }
      case _ if args.head.startsWith("-") => {
        val opt = args.head
        if (opt.contains("=")) {
          val kv = opt.split("=", 2)
          opts += kv(0) -> kv(1)
        } else {
          opts += opt -> ""
        }
        _getOpts(args.tail)
      }
      case _ => {
        args
      }
    }
    val rest = _getOpts(args)
    (opts, rest)
  }

}
