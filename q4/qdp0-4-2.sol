c clasp version 3.3.5
c Reading from q4/qdp0-4-2.opb
c Solving...
c Answer: 1
v -x1 -x2 -x3 -x4 -x5 -x6 -x7 -x8 x9 -x10 -x11 x12 -x13 -x14 -x15 -x16
c Answer: 2
v -x1 -x2 x3 -x4 -x5 -x6 -x7 -x8 -x9 -x10 -x11 -x12 -x13 -x14 x15 -x16
c Answer: 3
v -x1 -x2 -x3 -x4 -x5 -x6 x7 -x8 -x9 -x10 x11 -x12 -x13 -x14 -x15 -x16
c Answer: 4
v -x1 -x2 -x3 -x4 -x5 -x6 -x7 -x8 -x9 x10 x11 -x12 -x13 -x14 -x15 -x16
c Answer: 5
v -x1 x2 -x3 -x4 -x5 -x6 -x7 -x8 -x9 -x10 -x11 -x12 -x13 x14 -x15 -x16
c Answer: 6
v x1 -x2 -x3 -x4 -x5 -x6 -x7 -x8 -x9 -x10 x11 -x12 -x13 -x14 -x15 -x16
c Answer: 7
v -x1 -x2 -x3 -x4 x5 -x6 -x7 x8 -x9 -x10 -x11 -x12 -x13 -x14 -x15 -x16
c Answer: 8
v -x1 -x2 -x3 x4 -x5 -x6 -x7 -x8 -x9 x10 -x11 -x12 -x13 -x14 -x15 -x16
c Answer: 9
v -x1 -x2 -x3 -x4 -x5 -x6 x7 -x8 -x9 -x10 -x11 -x12 x13 -x14 -x15 -x16
c Answer: 10
v -x1 -x2 -x3 -x4 -x5 x6 x7 -x8 -x9 -x10 -x11 -x12 -x13 -x14 -x15 -x16
c Answer: 11
v -x1 -x2 -x3 -x4 -x5 x6 -x7 -x8 -x9 x10 -x11 -x12 -x13 -x14 -x15 -x16
c Answer: 12
v -x1 -x2 -x3 -x4 -x5 x6 -x7 -x8 -x9 -x10 -x11 -x12 -x13 -x14 -x15 x16
s SATISFIABLE
c 
c Models         : 12
c Calls          : 1
c Time           : 0.000s (Solving: 0.00s 1st Model: 0.00s Unsat: 0.00s)
c CPU Time       : 0.000s
